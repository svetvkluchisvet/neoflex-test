import {AiOutlineMinus} from "react-icons/ai";
import {BiPlus} from "react-icons/bi";
import {MdOutlineDeleteForever} from "react-icons/md";
import './Order.css'
export const Order = (data) => {
    const {img, amount, title, price} = data.data
    return (
        <div className="cart-card">
            <div>
                <div className="cart-image">
                    <img className="cart-img" src={process.env.PUBLIC_URL + '/images' + img}
                         alt="загрузка..."/>
                </div>

                <div className="button-container">
                    <button className="minus" onClick={() => data.setAmount(false)}><AiOutlineMinus/></button>
                    <div className="cart-count">{amount}</div>
                    <button className="plus" onClick={() => data.setAmount(true)}><BiPlus/></button>
                </div>
            </div>

            <div className='data-container'>
                <div className="name">{title}</div>
                <div className="cart-price">{price}</div>
            </div>

            <div className="delete-container">
                <MdOutlineDeleteForever className="cart-icon"/>
                <div className="big-cart-price">{price*amount} ₽</div>
            </div>

        </div>
    );
};