import './footer.css'
import {Link} from "react-router-dom";
import {ROUTES} from "../utils/routes";
import {RiTelegramLine, RiWhatsappLine} from "react-icons/ri";
import {SlSocialVkontakte} from "react-icons/sl";
import {MdLanguage} from "react-icons/md";

export const Footer = () => {
    return (
        <div className="footer">
            <div className="footer-logo">
                <Link to={ROUTES.HOME} className="text">
                    QPICK
                </Link>
            </div>

            <div className="footer-menu">
                <div className="first">
                    <Link to={ROUTES.FAVORITES} className="text">
                        Избранное
                    </Link>
                    <Link to={ROUTES.CART} className="text">
                        Корзина
                    </Link>
                    <Link to={ROUTES.CONTACTS} className="text">
                        Контакты
                    </Link>
                </div>
                <div className="second">
                    <Link to={ROUTES.TERMS} className="text">
                        Условия сервиса
                    </Link>
                    <div className="language">
                        <MdLanguage className="text"/>
                        <div className="text">
                            Рус
                        </div>
                        <div className="text">
                            Eng
                        </div>
                    </div>
                </div>
            </div>

            <div className="social">
                <Link to={'https://vk.com/svetvkluchisvet'} className="text">
                    <SlSocialVkontakte/>
                </Link>
                <Link to={"https://t.me/svetvkluchisvet"} className="text">
                    <RiTelegramLine/>
                </Link>
                <Link to={'https://www.whatsapp.com/'} className="text">
                    <RiWhatsappLine/>
                </Link>
            </div>
        </div>

    );
};