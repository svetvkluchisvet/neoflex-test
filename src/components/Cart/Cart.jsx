import './cart.css'
import {Order} from "./../Order/Order";
import React, {useState} from "react";

export const Cart = () => {
    const [amountInCart, setAmountInCart] = useState(sessionStorage.getItem("amount"))

    const [order, setOrder] = useState([
        {
            id: 1,
            img: '/AppleBYZS852I.png',
            title: 'Apple BYZ S8521',
            price: 2927,
            amount: 1,
            source: 'AppleBYZS852I.png'
        }
    ])

    const sumPrice = () => {
        let sum = 0
        order.map(data => {
            sum += data.amount * data.price
            return null
        })
        return sum
    }

    const changeAmount = (event, id) => {
        const newArray = [...order]
        newArray.map(item => {
            if (item.id === id) {
                // eslint-disable-next-line no-unused-expressions
                event ? item.amount += 1 :
                    (item.amount > 0) ?
                        item.amount -= 1 : null
            }
            return null;
        })
        setOrder(newArray)
    }


    return (
        <div className='container'>
            <div className="cart-text">Корзина</div>
            <div className="elements">
                <div>
                    {order.map(data => {
                       return <Order key={data.id} data={data} setAmount={(e) => changeAmount(e, data.id)}/>
                    })}
                </div>
                <div className="total-container">
                    <div className="buy">
                        <div className="total-text">ИТОГО</div>
                        <div className="total-price">₽ {sumPrice()}</div>
                    </div>
                    <button className='buy-button'>Перейти к оформлению</button>
                </div>
            </div>
        </div>
    );
};