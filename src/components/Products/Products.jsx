import './Products.css'
import {useContext, useState} from "react";
import {Card} from "../Card/Card";
import {AppContext} from "../../App";

const Headphones = [
    {
        id: 1,
        img: "/AppleBYZS852I.png",
        title: "Apple BYZ S852I",
        price: "2927",
        oldPrice: "3527",
        rate: "4.7",
    },
    {
        id: 2,
        img: "/AppleEarPods.png",
        title: "Apple EarPods",
        price: "2327",
        oldPrice: "",
        rate: "4.5",
    },
    {
        id: 3,
        img: "/AppleEarPodsBox.png",
        title: "Apple EarPods",
        price: "2327",
        oldPrice: "",
        rate: "4.5",
    },
    {
        id: 4,
        img: "/AppleBYZS852I.png",
        title: "Apple BYZ S852I",
        price: "2927",
        oldPrice: "",
        rate: "4.7",
    },
    {
        id: 5,
        img: "/AppleEarPods.png",
        title: "Apple EarPods",
        price: "2327",
        oldPrice: "",
        rate: "4.5",
    },
    {
        id: 6,
        img: "/AppleEarPodsBox.png",
        title: "Apple EarPods",
        price: "2327",
        oldPrice: "",
        rate: "4.5",
    },
];

const WirelessHeadphones = [
    {
        id: 1,
        img: "/AppleAirPods.png",
        title: "Apple EarPods",
        price: "9527",
        oldPrice: "",
        rate: "4.7",
    },
    {
        id: 2,
        img: "/GERLAXGH04.png",
        title: "GERLAX GH-04",
        price: "6527",
        oldPrice: "",
        rate: "4.7",
    },
    {
        id: 3,
        img: "/BOROFONEBO4.png",
        title: "BOROFONE BO4",
        price: "7527",
        oldPrice: "",
        rate: "4.7",
    },
];

export const Products = () => {
    const [orderInCart, setOrderInCart] = useState(sessionStorage.getItem("order"));
    const {setValue} = useContext(AppContext);

    const updateAmountInCart = () => {
        let order;
        order = sessionStorage.getItem("order");

        order = Number(order) + 1;

        sessionStorage.setItem("order", order);

        setOrderInCart(order);
        setValue(order);
    }

    return (
        <div>
            <p className="category">Наушники</p>
            <div className="products-list">
                {Headphones.map(data => {
                    return <Card key={data.id} data={data} setAmount={() => {
                        updateAmountInCart();
                    }}/>
                })}
            </div>
            <p className="category">Беспроводные наушники</p>

            <div className="products-list">
                {WirelessHeadphones.map(data => {
                    return <Card key={data.id} data={data} setAmount={() => {
                        updateAmountInCart();
                    }}/>
                })}
            </div>
        </div>
    );
};