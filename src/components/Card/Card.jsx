import {AiFillStar} from "react-icons/ai";
import './Card.css'
export const Card = (data) => {
    const {img, title, price, oldPrice, rate} = data.data;
    return (
        <div className="card">
            <div className="image">
                <img className="img" src={process.env.PUBLIC_URL + '/images' + img} alt="загрузка..."/>
            </div>
            <div className="wrapper">
                <div className="info1">
                    <div className="title">{title}</div>
                    <div className="prices">
                        <div className="price">{price} ₽</div>
                        <div className="oldPrice">{oldPrice}</div>
                    </div>
                </div>
                <div className="info2">
                    <div className="rateContainer">
                        <AiFillStar className="star"/>
                        <div className="rate">{rate}</div>
                    </div>
                    <button
                        className="buyButton"
                        onClick={() => data.setAmount()}>Купить
                    </button>
                </div>
            </div>
        </div>
    );
};