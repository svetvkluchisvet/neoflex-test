import {Routes, Route} from "react-router-dom";
import {MainPage} from "../MainPage/MainPage";
import {ROUTES} from "../utils/routes";
import {Cart} from "../Cart/Cart";

export const AppRoutes = () => {
    return (
        <Routes>
            <Route index element={<MainPage/>}/>
            <Route path={ROUTES.CART} element={<Cart/>}/>
        </Routes>
    );
};