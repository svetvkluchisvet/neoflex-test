export const ROUTES = {
    HOME: '',
    CART: '/cart',
    CONTACTS: '/contacts',
    FAVORITES: '/favorites',
    TERMS: '/terms',

}