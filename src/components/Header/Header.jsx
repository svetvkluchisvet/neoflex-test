import './header.css'
import {ROUTES} from "../utils/routes";
import {Link} from "react-router-dom";
import {AiOutlineHeart, AiOutlineShoppingCart} from "react-icons/ai";
import {useContext} from "react";
import {AppContext} from "../../App";

//https://gitlab.com/svetvkluchisvet/neoflex-test.git
export const Header = () => {
    const {value} = useContext(AppContext);
    return (
        <div className="header">
            <div className="logo">
                <Link to={ROUTES.HOME} className="text">
                    QPICK
                </Link>
            </div>

            <div className="account">
                <Link to={ROUTES.FAVORITES} className="favourites">
                    <AiOutlineHeart/>
                    <span className="count">3</span>
                </Link>
                <Link to={ROUTES.CART} className="cart">
                    <AiOutlineShoppingCart/>
                    <span className="count">{value || '1'}</span>
                </Link>
            </div>
        </div>
    );
};