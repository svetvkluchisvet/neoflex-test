import {Header} from "./components/Header/Header";
import {Footer} from "./components/Footer/Footer";
import React, {createContext, useState} from "react";
import {AppRoutes} from "./components/AppRoutes/AppRoutes";

export const AppContext = createContext(null);

function App() {
    const [value, setValue] = useState();

    return (
        <div>
            <AppContext.Provider value={{ value, setValue }}>
            <Header/>
               <AppRoutes/>
            </AppContext.Provider>
            <Footer/>
        </div>
    );
}

export default App;
